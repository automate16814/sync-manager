#!/usr/bin/env python

"""
Author: MobileNoc
Purpose: sync-manager util API's to get deivce information
"""
import logging
import os

log = logging.getLogger(__name__)
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"),
                    format='%(process)d-%(levelname)s-%(message)s')

def get_machine_id(host):
    "TDB: invoke python-api to fetch machine-id when supported"
    log.info("fetched machine_id '%s' for host '%s'" % (43, host))
    return 43


def get_network_os(host):
    "TDB: invoke python-api to fetch machine-id when supported"
    log.info("fetched network_os '%s' for host '%s'" % ('eos', host))
    return 'eos'


def get_running_config(machine_id):
    "TDB: get running-config using python-api"
    cfg_file_path = os.path.join(os.path.dirname(__file__), '../../tests/integration/fixtures/eos.cfg')
    log.info("loading device running-config from file %s" % cfg_file_path)
    with open(cfg_file_path) as fp:
        content = fp.read()
    log.info("fetched running-config from device for machine_id '%s'" % machine_id)
    return content