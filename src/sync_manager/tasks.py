#!/usr/bin/env python

"""
Author: MobileNoc
Purpose: sync-manager celery worker: config update to ES
"""
from __future__ import absolute_import, unicode_literals

import os
import logging

from celery import Celery

from sync_manager.config.base import get_option
from sync_manager.network import utils as nwutils
from sync_manager.network import config
from sync_manager.es import utils as esutils

broker = get_option('broker', section='celery')
app = Celery('tasks', broker=broker)

log = logging.getLogger(__name__)
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"),
                    format='%(process)d-%(levelname)s-%(message)s')

@app.task
def config_sync(host):
    log.info('syncing config from host %s to elastic search' % host)
    machine_id = nwutils.get_machine_id(host)
    network_os = nwutils.get_network_os(host)
    config_on_device = nwutils.get_running_config(machine_id)
    config_on_es = esutils.get_running_config(machine_id)
    diff = config.get_diff(config_on_device, config_on_es, network_os)
    if diff:
        esutils.update_running_config(config_on_device, machine_id, action='update')
    else:
        log.info("config in ES is in sync with that on network machine %s" % machine_id)