#!/usr/bin/env python

"""
Author: MobileNoc
Purpose: sync-manager util API's to get information from ES
"""
import logging
import os
import requests

from sync_manager.config.base import get_option

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import NotFoundError

es = Elasticsearch()
ES_HOST = get_option('host', section='elasticsearch')
ES_PORT = get_option('port', section='elasticsearch')

log = logging.getLogger(__name__)
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"),
                    format='%(process)d-%(levelname)s-%(message)s')

BASE_URL = "%s:%s" % (ES_HOST, ES_PORT)
ES = Elasticsearch()


def get_running_config(machine_id):
    log.info("fetched running-config from Elastic search for machine_id '%s'" % machine_id)
    try:
        response = ES.get(index='machines', doc_type='running_config_text', id=machine_id)
        config = response
    except NotFoundError:
        log.info("config for machine-id %s not found" % machine_id)
        config = ''

    log.info('es get config for machine-id %s: %s' % (machine_id, config))
    return config

def update_running_config(config, machine_id, action='update'):
    if action == 'update':
        res = es.create(index="machines", id=machine_id, doc_type="running_config_text", body={"content": config})
        log.info('ES create: %s' % res)
