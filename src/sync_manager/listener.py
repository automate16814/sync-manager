#!/usr/bin/env python

"""
Author: MobileNoc
Purpose: sync-manager config change listener
"""
from sync_manager.config.base import get_option
try:
    from provider.snmp import trap
    HAS_SNMP_TRAP = True
except ImportError:
    HAS_SNMP_TRAP = False


def run_listener():
    provider = get_option('provider')
    if provider == 'snmp':
        if not HAS_SNMP_TRAP:
            raise Exception("provider '%s' is not supported" % provider)
        trap.run()
    else:
        raise ("invalid provider value '%s'" % provider)


if __name__ == '__main__':
    run_listener()