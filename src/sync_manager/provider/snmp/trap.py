#!/usr/bin/env python

"""
Author: MobileNoc
Purpose: sync-manager snmp trap receiver.
"""
import logging

from sync_manager.config.base import get_option
from sync_manager.tasks import config_sync
import os
try:
    from pysnmp.entity import engine, config
    from pysnmp.carrier.asyncore.dgram import udp
    from pysnmp.entity.rfc3413 import ntfrcv
    from pysnmp.smi import builder, view, compiler, rfc1902, error
except ImportError:
    raise Exception('pysnmp library is not installed')

log = logging.getLogger(__name__)
logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"),
                    format='%(process)d-%(levelname)s-%(message)s')

MIB_VIEW_CONTROLLER = None
PDU_COUNT = 1
ARISTA_EOS_CONFIG_TRAP = {}


def handle_arista_eos_config_trap(exec_context):
    global ARISTA_EOS_CONFIG_TRAP
    ip_addr = str(exec_context['transportAddress'][0])
    if ip_addr in ARISTA_EOS_CONFIG_TRAP.keys():
        prompt_mode = ARISTA_EOS_CONFIG_TRAP[ip_addr]['prompt_mode']
        # exit trap received
        if prompt_mode == 'config':
            ARISTA_EOS_CONFIG_TRAP[ip_addr]['prompt_mode'] = 'oper'
            log.info("Received config mode exit trap for host %s" % ip_addr)
            log.info("[%s] post config trap for processing by celery worker" % ip_addr)
            result = config_sync.delay(ip_addr)
        elif prompt_mode == 'oper':
            # prompt changed to config mode
            log.info("Received config mode enter trap for host %s" % ip_addr)
            ARISTA_EOS_CONFIG_TRAP[ip_addr]['prompt_mode'] = 'config'
        else:
            raise Exception("arista eos invalid prompt mode %s" % prompt_mode)
    else:
        log.info("Initial config mode enter trap for host %s" % ip_addr)
        ARISTA_EOS_CONFIG_TRAP[ip_addr] = {'prompt_mode': 'config'}


def mib_builder(mib_path, mib_modules):
    mib_builder = builder.MibBuilder()
    try:
        if mib_path:
            mib_path_list = []
            for mibp in mib_path.split(','):
                mib_path_list.append(os.path.join(os.path.dirname(__file__), mibp))

            custom_mib_path = get_option('custom_mib_path', section='snmp')
            for mibp in custom_mib_path.split(','):
                mib_path_list.append(mibp)
            log.info("Load mibs from path %s" % ', '.join(mib_path_list))
            compiler.addMibCompiler(mib_builder, sources=mib_path_list)

        global MIB_VIEW_CONTROLLER
        MIB_VIEW_CONTROLLER = view.MibViewController(mib_builder)
        if mib_modules:
            _mibs=mib_modules.split(",")
            mib_builder.loadModules(*_mibs)
    except error.MibNotFoundError as excep:
        log.error(" {} Mib Not Found!".format(excep))


def trap_callback(snmp_engine, state_reference, context_engine_id, context_name, var_binds, cbctx):
    global PDU_COUNT
    global MIB_VIEW_CONTROLLER
    config_modules = get_option('config_modules', section='snmp')
    is_config_trap = False
    log.info("----- NEW Notification(pdu_count: {}) -----".format(PDU_COUNT))
    exec_context = snmp_engine.observer.getExecutionContext(
        'rfc3412.receiveMessage:request'
    )
    log.info('#Notification from %s \n#context_engine_id: "%s" \n#context_name: "%s" \n#SNMPVER "%s" \n#security_name "%s"' %
             ('@'.join([str(x) for x in exec_context['transportAddress']]), context_engine_id.prettyPrint(),
              context_name.prettyPrint(), exec_context['securityModel'], exec_context['securityName']))

    for oid, val in var_binds:
        output = rfc1902.ObjectType(rfc1902.ObjectIdentity(oid),val).resolveWithMib(MIB_VIEW_CONTROLLER).prettyPrint()
        log.debug(output)
        if 'ARISTA-CONFIG-MAN-MIB' in output:
            handle_arista_eos_config_trap(exec_context)
            break
    PDU_COUNT += 1


def run():
    snmp_engine = engine.SnmpEngine()
    # Configure community here
    community_string = get_option('community', section='snmp')
    community_string_type = get_option('community_string_type', section='snmp')
    config.addV1System(snmp_engine, community_string, community_string_type)

    # load mibs
    mib_path = get_option('mib_path', section='snmp')
    mib_modules = get_option('mib_modules', section='snmp')
    mib_builder(mib_path, mib_modules)

    # start trap listener
    trap_receiver_address = get_option('trap_receiver_address', section='snmp')
    trap_receiver_port = int(get_option('trap_receiver_port', section='snmp'))

    ntfrcv.NotificationReceiver(snmp_engine, trap_callback)
    log.info("Agent is listening SNMP Trap on %s, Port: %s" % (trap_receiver_address, trap_receiver_port))
    try:
        config.addTransport(
            snmp_engine,
            udp.domainName,
            udp.UdpTransport().openServerMode((trap_receiver_address, trap_receiver_port))
        )
    except Exception as e:
        log.info("{} Port Binding Failed the Provided Port {} is in Use".format(e, trap_receiver_port))

    snmp_engine.transportDispatcher.jobStarted(1)

    try:
        log.info("Trap Listener started .....")
        log.info("To Stop Press Ctrl+c")
        log.info("\n")
        snmp_engine.transportDispatcher.runDispatcher()
    except:
        snmp_engine.transportDispatcher.closeDispatcher()
        raise
