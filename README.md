sync-manager
------------
Pre-requisite:
1. Run rabitmq-server and elastic-search on local host

Installation dependent library:
```buildoutcfg
pip install -r  requirements.txt
source env-setup.sh
```
Trap listener setup:
```buildoutcfg
sudo --preserve-env=PYTHONPATH python src/sync_manager/listener.py 
```
Celery worker setup:
```buildoutcfg
celery -A sync_manager.tasks worker --loglevel=info
```
